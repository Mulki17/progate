const age = 17;

// Ketika kondisi tidak terpenuhi, cetak "Saya berusia di bawah 18 tahun"
if (age >= 18) {
    console.log("Saya berusia diatas 18 tahun");
} else {
    console.log("Saya berusia dibawah 18 tahun");
}

// ELSE IF
const agee = 17;

/* Ketika agee lebih besar atau sama dengan 10, cetak:
"Saya berusia di bawah 18 tahun, namun di atas 9 tahun" */
if (agee >= 18) {
    console.log("Saya di atas 18 tahun");
} else if (agee >= 10) {
    console.log("Saya berusia di bawah 18 tahun, namun di atas 9 tahun");
} else {
    console.log("saya di bawah 10 tahun");
}

// PERSYARATAN
const age3 = 24;

// Tambahkan pernyataan if dengan kondisi yang telah ditentukan
if (age3 >= 20 && age3 < 30) {
    console.log("Saya di usia 20-an tahun");
}