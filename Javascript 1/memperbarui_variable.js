let name = "Ninja Ken";
console.log(name);

// Update nilai variable ke "Birdie"
name = "Birdie";

// Cetak nilai dari variable name
console.log(name);



// Memperbarui Variable Menggunakan Nilainya
let number = 7;
console.log(number);

// Tambahkan 3 ke nilai variable number
number = number + 3;

console.log(number);

// Bagi nilai variable number dengan 2
number = number / 2;

console.log(number);