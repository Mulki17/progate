// Cetak hasil 5 tambah 3
console.log(5 + 3);

// Cetak hasil 20 kurang 8
console.log(20 - 8);

// Cetak string "4 + 5"
console.log("4 + 5");

// Cetak hasil 8 kali 4 di console.
console.log(8 * 4);

// Cetak hasil 24 bagi 4 di console.
console.log(24 / 4);

// Cetak sisa setelah membagi 7 dengan 2 di console.
console.log(7 % 2);